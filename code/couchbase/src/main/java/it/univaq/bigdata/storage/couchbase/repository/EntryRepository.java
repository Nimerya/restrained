package it.univaq.bigdata.storage.couchbase.repository;

import it.univaq.bigdata.storage.couchbase.model.Entry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntryRepository extends CrudRepository<Entry, String> {

    //Don't need to re-declared update, save, delete and findAll()

    /* If you need to submit a special query, wrote it in the
    @Query annotation and then declare the statement to call
     */
}
