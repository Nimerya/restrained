package it.univaq.bigdata.storage.couchbase.service;

import it.univaq.bigdata.storage.couchbase.model.Entry;

public interface EntryService {

    Entry save(Entry e);
}
