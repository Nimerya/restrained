package it.univaq.bigdata.storage.couchbase.service;

import it.univaq.bigdata.storage.couchbase.model.Entry;
import it.univaq.bigdata.storage.couchbase.repository.EntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EntryServiceImpl implements EntryService{
    @Autowired
    EntryRepository entryRepository;

    public Entry save(Entry e){
        return entryRepository.save(e);
    }
}
