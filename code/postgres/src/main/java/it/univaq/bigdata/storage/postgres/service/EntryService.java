package it.univaq.bigdata.storage.postgres.service;


import it.univaq.bigdata.storage.postgres.model.Entry;

public interface EntryService {
    Entry save(Entry e);

}
