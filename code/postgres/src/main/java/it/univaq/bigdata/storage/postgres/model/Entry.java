package it.univaq.bigdata.storage.postgres.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "entries")
public class Entry {

    @Id
    private String metaId = UUID.randomUUID().toString();

    @Column
    private String value;

    public Entry(){}

    public Entry(String metaId, String value) {
        this.metaId = metaId;
        this.value = value;
    }

    public String getMetaId() {
        return metaId;
    }

    public void setMetaId(String metaId) {
        this.metaId = metaId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
