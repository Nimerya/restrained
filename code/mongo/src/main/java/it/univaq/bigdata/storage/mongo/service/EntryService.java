package it.univaq.bigdata.storage.mongo.service;

import it.univaq.bigdata.storage.mongo.model.Entry;

public interface EntryService {

    Entry save(Entry e);
}
