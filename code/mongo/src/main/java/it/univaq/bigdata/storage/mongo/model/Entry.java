package it.univaq.bigdata.storage.mongo.model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document(collection="restrained.coll")
public class Entry {

    private String metaId = UUID.randomUUID().toString();

    private String value;


    public Entry(){}

    public String getMetaId() {
        return metaId;
    }

    public void setMetaId(String metaId) {
        this.metaId = metaId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
