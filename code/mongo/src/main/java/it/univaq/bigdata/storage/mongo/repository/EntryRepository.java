package it.univaq.bigdata.storage.mongo.repository;

import it.univaq.bigdata.storage.mongo.model.Entry;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface EntryRepository extends MongoRepository<Entry, String> {

    //Don't need to re-declared update, save, delete and findAll()

    /* If you need to submit a special query, wrote it in the
    @Query annotation and then declare the statement to call
     */

}
