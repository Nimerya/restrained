package it.univaq.bigdata.storage.cassandra.service;

import it.univaq.bigdata.storage.cassandra.model.Entry;

public interface EntryService {
    Entry save(Entry e);

}
