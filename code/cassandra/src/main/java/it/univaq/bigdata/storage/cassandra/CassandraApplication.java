package it.univaq.bigdata.storage.cassandra;

import it.univaq.bigdata.storage.cassandra.model.Entry;
import it.univaq.bigdata.storage.cassandra.service.EntryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class CassandraApplication {

	@Autowired
	EntryServiceImpl entryService;

	public static void main(String[] args) {
		SpringApplication.run(CassandraApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void start(){

		// Here must be the reader of the file where are all the data

		Entry e = new Entry();
		e.setValue("180.502959");

		entryService.save(e);
	}
}
