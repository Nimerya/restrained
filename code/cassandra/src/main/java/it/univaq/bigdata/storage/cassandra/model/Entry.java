package it.univaq.bigdata.storage.cassandra.model;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Table
public class Entry {

    @PrimaryKey
    private String metaId = UUID.randomUUID().toString();

    private String value;

    public Entry(){}

    public Entry(String metaId, String value) {
        this.metaId = metaId;
        this.value = value;
    }

    public String getMetaId() {
        return metaId;
    }

    public void setMetaId(String metaId) {
        this.metaId = metaId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
