Adding shard node URL: jdbc:postgresql://127.0.0.1:5432/ycsb
Adding shard node URL: jdbc:postgresql://127.0.0.1:5432/ycsb
Adding shard node URL: jdbc:postgresql://127.0.0.1:5432/ycsb
Adding shard node URL: jdbc:postgresql://127.0.0.1:5432/ycsb
Adding shard node URL: jdbc:postgresql://127.0.0.1:5432/ycsb
Adding shard node URL: jdbc:postgresql://127.0.0.1:5432/ycsb
Using shards: 1, batchSize:-1, fetchSize: -1
Using shards: 1, batchSize:-1, fetchSize: -1
Using shards: 1, batchSize:-1, fetchSize: -1
Using shards: 1, batchSize:-1, fetchSize: -1
Using shards: 1, batchSize:-1, fetchSize: -1
Using shards: 1, batchSize:-1, fetchSize: -1
[OVERALL], RunTime(ms), 546892
[OVERALL], Throughput(ops/sec), 18285.14587889382
[TOTAL_GCS_G1_Young_Generation], Count, 1307
[TOTAL_GC_TIME_G1_Young_Generation], Time(ms), 1000
[TOTAL_GC_TIME_%_G1_Young_Generation], Time(%), 0.18285145878893821
[TOTAL_GCS_G1_Old_Generation], Count, 0
[TOTAL_GC_TIME_G1_Old_Generation], Time(ms), 0
[TOTAL_GC_TIME_%_G1_Old_Generation], Time(%), 0.0
[TOTAL_GCs], Count, 1307
[TOTAL_GC_TIME], Time(ms), 1000
[TOTAL_GC_TIME_%], Time(%), 0.18285145878893821
[READ], Operations, 9500368
[READ], AverageLatency(us), 209.42080085739838
[READ], MinLatency(us), 53
[READ], MaxLatency(us), 195199
[READ], 95thPercentileLatency(us), 569
[READ], 99thPercentileLatency(us), 895
[READ], Return=OK, 9500368
[CLEANUP], Operations, 6
[CLEANUP], AverageLatency(us), 513.1666666666666
[CLEANUP], MinLatency(us), 137
[CLEANUP], MaxLatency(us), 2279
[CLEANUP], 95thPercentileLatency(us), 2279
[CLEANUP], 99thPercentileLatency(us), 2279
[INSERT], Operations, 499632
[INSERT], AverageLatency(us), 2520.469143289461
[INSERT], MinLatency(us), 587
[INSERT], MaxLatency(us), 196095
[INSERT], 95thPercentileLatency(us), 3533
[INSERT], 99thPercentileLatency(us), 44543
[INSERT], Return=OK, 499632
